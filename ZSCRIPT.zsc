version "4.11"

class DSMauler : DSFirearm
{
	override void OnSpawn(Actor grabber, int type, int arg)
	{
		Actor a = Spawn('DSBuckshotBox', Vec3Angle(3, Angle));
		a.Master = grabber;
		Super.OnSpawn(grabber, type);
	}
	
	override void InitData(DSData data)
	{
		Super.InitData(data);

		InitItemMag('DSMaulMag', 'DSMaulMag');
		InitChamber(null);
	}

	override int AssessPriority(WeaponData data)
	{
		let fd = FirearmData(data);
		return int(APriority_Low * (fd.Chambers[0] ? fd.Chambers[0].GetPriorityMultiplier(Targeting) : 1.0)) + 2;
	}
	override bool HasAmmo(WeaponData data)
	{
		let fd = FirearmData(data);
		let mag = MagData(fd.Data[0]);
		return fd.Chambers[0] && !fd.Chambers[0].Spent || mag && mag.GetAmmoCount() >= fd.GetAmmoToUse(mag);
	}

	override double GetWeight(DSData data) { return 6; }
	override string GetIcon(DSData data) { return data.Data[0] ? "JMWGB0" : "JMWGA0"; }
	override string GetHelpText(DSData data)
	{
		return DSMenu.GetKeybinds("+attack").."\n  Shoot\n"
		..DSMenu.GetKeybinds("+reload")..HELPTEXT_HOLD.."\n  Reload mode\n"
		.."    "..HELPTEXT_REL_HOLD..HELPTEXT_REL_SW.."\n      Magazine selector\n"
		.."        "..HELPTEXT_REL_EXIT..HELPTEXT_REL_ANY.."\n          Insert magazine\n"
		.."    "..HELPTEXT_REL_S.."\n      Load/eject chamber\n"
		.."    "..HELPTEXT_REL_SE.."\n      Eject magazine\n"
		.."    "..HELPTEXT_REL_HOLD..HELPTEXT_REL_SE.."\n      Catch ejected mag/shell";
	}
	override string GetLore(DSData data) { return "A concept weapon built entirely with the idea of being stupidly inefficient but nevertheless fun to use. The SoSA-450 is a box magazine-fed LMG firing shotgun shells. The SoSA-450A2 improved the firearm with the ability to load other shell loads such as slugs."; }

	override Vector2 DrawMagDisplay(DSHud hud, DSPlayer plr, DSData data, Vector2 pos, int flags)
	{
		hud.DrawImage("JMWMAGBG", pos - (45, 16), flags | hud.DI_ITEM_RIGHT_BOTTOM, hud.DimAlpha, col: hud.HudCol);

		let fd = FirearmData(data);

		let ch = fd.Chambers[0];
		if (ch)
		{
			hud.DrawImage(ch.Spent ? "JMWMAGSP" : "JMWMAGCH", pos - (47, 18), flags | hud.DI_ITEM_RIGHT_BOTTOM, col: ch.AmmoColorHud);
		}

		let md = MagData(fd.Data[0]);
		if (md)
		{
			md.DrawMagazineBar(hud, pos - (51, 18), 33, 1, flags);
		}

		let tex = TexMan.CheckForTexture("JMWMAGFG", TexMan.Type_MiscPatch);
		hud.DrawTexture(tex, pos - (45, 16), flags | hud.DI_ITEM_RIGHT_BOTTOM, col: hud.HudCol);

		return TexMan.GetScaledSize(tex);
	}

	override void OnReloadAreaEnter(int side)
	{
		let fd = FirearmData(Data[0]);
		if (side == 1 && A_RemoveMag(fd.Data[0], flags: RMF_DROP))
		{
			A_StartSound("JackMaul/MagOut", 8, CHANF_OVERLAP, attenuation: 2);
		}
		if (side == 0)
		{
			A_TryChamberOrEject(fd.Chambers[0], fd.Data[0], fd.GetSelectedAmmo(DSData.AAIndex_Ammo), "JackMaul/BoltBack", "JackMaul/BoltForward", sndDelay: 6);
		}
	}
	override void OnReloadAreaExit(int side)
	{
		let fd = FirearmData(Data[0]);
		let [stack, index] = GetMagStack(fd);
		if (WepFlags & DSWF_INMAGMODE && DSMagAmmo.InsertMag(fd, fd.Data[0], stack, index))
		{
			A_StartSound("JackMaul/MagIn", 8, attenuation: 2);
		}
	}

	action void A_FireMauler(int stage)
	{
		let fd = FirearmData(invoker.Data[0]);

		A_SwayRotation();

		switch (stage)
		{
			case 0:
			{
				let ch = fd.Chambers[0];
				for (int i = 0; i < ch.ProjectileCount; ++i)
				{
					DSProjectile.Fire(invoker, fd, ch, null, i, i > 0 ? (0.5, 0.5) * ch.AmmoProps.SpreadFactor : (0, 0));
				}

				A_StartSound("JackMaul/Fire", CHAN_WEAPON);
				A_WeaponAlert();
				A_ZoomRecoil(5);
				A_MuzzleClimb((frandom(-1.3, 1.3), -frandom(2.0, 3.0)));
				A_Light1();
				fd.Heat += 2;
				A_OverlayScale(OverlayID(), 0.2, 0.2, WOF_ADD);
				A_WeaponOffset(0, 6, WOF_ADD);

				A_SetShootState();
				break;
			}
			case 1:
			{
				A_EjectChamber(fd.Chambers[0]);
				A_OverlayScale(OverlayID(), -0.1, -0.1, WOF_ADD);
				A_WeaponOffset(0, -3, WOF_ADD);
				A_Light0();
				break;
			}
			case 2:
			{
				A_ChamberAmmoMag(fd.Chambers[0], fd.Data[0]);
				A_OverlayScale(OverlayID(), -0.1, -0.1, WOF_ADD);
				A_WeaponOffset(0, -3, WOF_ADD);
				break;
			}
		}
	}

	Default
	{
		Scale 0.25;
		Tag "SoSA-450A2 'Mauler'";
		Inventory.Icon "JMWGB0";
		Weapon.SlotNumber 3;
		Weapon.SlotPriority 2;
		DSWeapon.CrosshairGraphic "JMWFRONT", "JMWBACK";
		DSWeapon.ModId 'SoSA';
	}

	States
	{
		Spawn:
			JMWG # -1 NoDelay
			{
				frame = invoker.Data[0].Data[0] ? 1 : 0;
			}
			Stop;
		Ready:
			JMWS A 1
			{
				A_SwayRotation();
				A_WeaponReady(WRF_ALLOWRELOAD);
			}
			Loop;
		Select:
			JMWS A 0;
			Goto Select0Big;
		Deselect:
			#### A 0;
			Goto Deselect0Big;

		Fire:
			#### A 0
			{
				let fd = FirearmData(invoker.Data[0]);
				return A_JumpIf(fd.Chambers[0] && !fd.Chambers[0].Spent, 'Shoot');
			}
			Goto Nope;
		Shoot:
			JMWF A 1 Bright A_FireMauler(0);
			JMWF B 1 A_FireMauler(1);
			JMWF C 1 A_FireMauler(2);
			JMWS A 2;
			#### A 0 A_Refire('Fire');
			Goto Ready;

		Reload:
			#### # 1
			{
				A_ControlWeapon();
				A_SwayRotation();
			}
			Loop;

		HeldNpc:
			#### T 1 A_JumpIf(IsPressingRemote(BT_ATTACK), 'RemoteFire');
			Loop;
		RemoteFire:
			#### # 1
			{
				invoker.BurstIndex = 0;
				let fd = FirearmData(invoker.Data[0]);
				if (A_NpcTryShoot(fd.Chambers[0], fd.Data[0]) == NTSResult_NoAmmo)
				{
					A_NpcTryReload(fd.Data[0], fd.GetSelectedAmmo(DSData.AAIndex_Mag));
				}
			}
			Goto HeldNpc;
		RemoteShoot:
			#### # 1 A_FireMauler(0);
			#### # 1 A_FireMauler(1);
			#### # 1 A_FireMauler(2);
			#### # 2;
			Goto RemoteFire;
		RemoteReload:
			#### # 5;
			#### # 4
			{
				let fd = FirearmData(invoker.Data[0]);
				if (A_RemoveMag(fd.Data[0]))
				{
					A_StartSound("JackMaul/MagOut", 8, CHANF_OVERLAP, attenuation: 2);
				}
			}
			#### # 23 A_StartSound("Inventory/Pockets", 9, attenuation: 2);
			#### # 11
			{
				let fd = FirearmData(invoker.Data[0]);
				let [stack, index] = invoker.GetMagStack(fd);
				if (DSMagAmmo.InsertMag(fd, fd.Data[0], stack, index))
				{
					A_StartSound("JackMaul/MagIn", 8, CHANF_OVERLAP, attenuation: 2);
				}
			}
			#### # 0
			{
				let fd = FirearmData(invoker.Data[0]);
				let mag = MagData(fd.Data[0]);
				return A_JumpIf((!fd.Chambers[0] || fd.Chambers[0].Spent) && mag && mag.GetAmmoCount() > 0, 'RemoteChamber');
			}
			Goto RemoteFire;
		RemoteChamber:
			#### # 4;
			#### # 6
			{
				let fd = FirearmData(invoker.Data[0]);
				A_TryChamberOrEject(fd.Chambers[0], fd.Data[0], fd.GetSelectedAmmo(DSData.AAIndex_Ammo), "JackMaul/BoltBack", "JackMaul/BoltForward", sndDelay: 6);
			}
			Goto RemoteFire;
	}
}

class DSMaulMag : DSRoundMag
{
	override double GetWeight(DSData data) { return 0.5; }
	override string GetIcon(DSData data) { return MagData(data).GetAmmoCount() > 0 ? "JMMGA0" : "JMMGB0"; }
	override string GetLore(DSData data) { return "50 rounds of 12 Gauge shooting goodness. Perfect for turning anything on the receiving end into a fine red mist, given you have the ammo."; }

	Default
	{
		Scale 0.5;
		Tag "'Mauler' Magazine";
		Inventory.Icon "JMMGA0";
		DSRoundMag.AmmoClass 'DSBuckshotAmmo';
		DSMagAmmo.Capacity 50;
	}

	States
	{
		Spawn:
			JMMG A -1;
			Stop;
		SpawnEmpty:
			JMMG B 0;
			Goto Super::SpawnEmpty;
	}
}

class DSMauler_Store : DSStoreItem
{
	override double GetCost() { return IPClass_MidGame; }
}

class DSMaulMag_Store : DSStoreItem
{
	override double GetCost() { return 80; }
	override double, int GetResupplyAmount() { return 0.015, 1; }
}
